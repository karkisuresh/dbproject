import os
from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.sql import functions as func
from pyspark.sql.types import *
from pyspark.sql.window import Window
from pyspark.ml.feature import VectorAssembler
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from preprocess import write_to_hdfs


# os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3"
# os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3"

conf = SparkConf().setAppName("DBProject")
sc = SparkContext(conf=conf)
sql = SQLContext(sc)

hdfs_path = 'hdfs://localhost:9000'


def load_data(path):
    train_path = os.path.join(path, 'training_data.csv')
    test_path = os.path.join(path, 'test_data.csv')
    train_data = sql.read.csv(path=train_path, header=True, inferSchema=True)
    test_data = sql.read.csv(path=test_path, header=True, inferSchema=True)
    return train_data, test_data


def moving_average_indicator(df):
    shorter = 3
    longer = 10
    # create window
    window_shorter = Window.partitionBy('company').orderBy('date').rowsBetween(-shorter, 0)
    window_longer = Window.partitionBy('company').orderBy('date').rowsBetween(-longer, 0)
    df = df.withColumn("typicalPrice", (df.high + df.low + df.close)/3)
    df = df.withColumn("shortMA", func.avg(df.typicalPrice).over(window_shorter))
    df = df.withColumn("longMA", func.avg(df.typicalPrice).over(window_longer))
    df = df.withColumn("delta", df.shortMA - df.longMA)
    # using crossover strategy to generate buy/sell signal (0 = sell, 1 = buy, 2 = hold)
    crossover = func.udf(lambda delta: 1 if delta > 0 else 0 if delta < 0 else 2, IntegerType())
    df = df.withColumn("label", crossover(df.delta))
    return df


def get_features_and_label(df):
    stages = []
    numeric_cols = ['close', 'volume', 'open', 'high', 'low', 'shortMA', 'longMA']
    assembler = VectorAssembler(inputCols=numeric_cols, outputCol="features")
    stages += [assembler]
    pipeline = Pipeline(stages=stages)
    pipeline_model = pipeline.fit(df)
    df = pipeline_model.transform(df)
    return df


if __name__ == '__main__':
    train_df, test_df = load_data(hdfs_path)
    new_df = moving_average_indicator(train_df)
    train = get_features_and_label(new_df)
    new_test_df = moving_average_indicator(test_df)
    test = get_features_and_label(new_test_df)
    model = DecisionTreeClassifier(featuresCol='features', labelCol='label', maxDepth=15)
    dt_model = model.fit(train)
    model_file = open('model', 'w+')
    replace = {'f'}
    print(dt_model.toDebugString, file=model_file)
    model_file.close()
    predictions = dt_model.transform(test)
    predictions = predictions.select('date', 'close', 'volume', 'open', 'high', 'low', 'company', 'prediction')
    predictions.toPandas().to_csv('predictions.csv', index=False)
    write_to_hdfs('predictions.csv')
    write_to_hdfs('model')
