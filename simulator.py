import os
import math
import random
from pyspark import SparkContext, SparkConf, SQLContext
from preprocess import write_to_hdfs

# os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3"
# os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3"

conf = SparkConf().setAppName("DBProject")
sc = SparkContext(conf=conf)
sql = SQLContext(sc)

hdfs_path = 'hdfs://localhost:9000'
write_to = open('result.txt', 'w+')


class Simulator:

    def __init__(self, data):
        self.balance = 10000000
        print("Starting balance: " + str(self.balance), file=write_to, flush=True)
        self.stock = {}
        self.df = data

    def trade_stocks(self, trading_date, to_predict):
        if not self.stock:
            data = self.df.filter(self.df.date == trading_date.date).filter(self.df[to_predict] == 1.0)
            if not data.count():
                return
            selected = random.choice(data.collect())
            no_of_stocks = math.floor(self.balance/selected.open)
            self.stock[selected.company] = no_of_stocks
            self.balance = self.balance - (no_of_stocks * selected.open)
            print("Transaction for " + trading_date.date + ":", file=write_to, flush=True)
            print("Bought " + str(no_of_stocks) + " stocks of " + selected.company + " for opening price of "
                  + str(selected.open), file=write_to, flush=True)
            print("balance: " + str(self.balance), file=write_to, flush=True)
        else:
            company = list(self.stock.keys())[0]
            data = self.df.filter(self.df.date == trading_date.date)\
                .filter(self.df.company == company).filter(self.df[to_predict] == 0.0)
            if data.count():
                close = data.first().close
                print("Transaction for " + trading_date.date + ":", file=write_to, flush=True)
                print("Selling the stocks of " + company + " for closing price of " + str(close), file=write_to, flush=True)
                self.balance += self.balance + (self.stock[company] * close)
                self.stock.clear()
                print("balance: " + str(self.balance), file=write_to, flush=True)


if __name__ == '__main__':
    predictions = sql.read.csv(path=os.path.join(hdfs_path, 'predictions.csv'), inferSchema=True, header=True)
    df = predictions.orderBy('date')
    dates = df.select('date').distinct().collect()
    traders = {'Machine': 'prediction'}
    for trader, predictor in traders.items():
        # print("Trading done by: " + trader)
        sim = Simulator(df)
        for date in dates:
            sim.trade_stocks(date, predictor)
        if sim.stock:
            comp = list(sim.stock.keys())[0]
            last_data = sim.df.filter(sim.df.date == dates[-1].date).filter(sim.df.company == comp)
            closing_price = last_data.first().close
            date = last_data.first().date
            print("Transaction for " + date + ":", file=write_to, flush=True)
            print("Selling the stocks of " + comp + " for closing price of " + str(closing_price), file=write_to, flush=True)
            final_balance = sim.balance + (sim.stock[comp] * closing_price)
        else:
            final_balance = sim.balance
        print("Final balance: " + str(final_balance), file=write_to, flush=True)
        print('-----------------------------------------------------', file=write_to, flush=True)
    write_to_hdfs("result.txt")
    write_to.close()


