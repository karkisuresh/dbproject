import os
import pandas as pd
import argparse
import warnings
from datetime import datetime
from subprocess import Popen, PIPE

warnings.filterwarnings("ignore")


def convert_date(date):
    try:
        date = datetime.strptime(date, '%Y/%m/%d')
    except ValueError:
        date = datetime.strptime(date, '%m/%d/%Y')
    return datetime.strftime(date, '%Y/%m/%d')


def preprocess_data(data_path):
    print('Preprocessing data....')
    dfs = []
    for directory in os.listdir(data_path):
        files = os.listdir(os.path.join(data_path, directory))
        for file in files:
            if not file.endswith(".csv") and not file.endswith(".dat") and not file.endswith(".txt"):
                continue
            sep = ','
            file_path = os.path.join(os.path.join(data_path, directory), file)
            # print(file_path)
            with open(file_path) as f:
                if ',' not in f.readline():
                    sep = ' '
            temp_df = pd.read_csv(file_path, index_col=None, header=0, sep=sep)
            company = file
            if file.startswith('HistoricalQuotes'):
                company = file.split('_')[1]
            company = company.split(".")[0]
            temp_df['company'] = company
            # print(temp_df.head(5))
            dfs.append(temp_df)
    df = pd.concat(dfs, axis=0, ignore_index=True, sort=False)
    train_df = df[df['date'].str.contains("2017")]
    test_df = df[df['date'].str.contains("2018")]
    train_df['date'] = train_df['date'].apply(convert_date)
    test_df['date'] = test_df['date'].apply(convert_date)
    print('Writing data to file....')
    train_df.to_csv("training_data.csv", index=False)
    test_df.to_csv("test_data.csv", index=False)
    print('Preprocessing complete....')


def write_to_hdfs(pattern):
    current_dir = os.getcwd()
    cmd = ["hdfs dfs -put -f " + os.path.join(current_dir, pattern) + " /"]
    process = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = process.communicate()
    if err:
        print("Error occurred while writing to HDFS automatically. Please run the script provided manually "
              "and make sure you have set up the PATH properly.")
        print(err)
    else:
        print("Complete............")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path', help="path of the directory containing dataset")
    args = parser.parse_args()
    preprocess_data(args.data_path)
    print("Writing to HDFs...........")
    write_to_hdfs('*.csv')

